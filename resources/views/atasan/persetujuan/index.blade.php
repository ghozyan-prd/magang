@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Users <small>Some examples to get you started</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
<div class="col-md-12 col-sm-12 ">
  <div class="x_panel">
    <div class="x_title">
      <h2>Daftar Request Cuti  <small>PT Davinti Indonesia</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
              <div class="card-box table-responsive">
      <p class="text-muted font-13 m-b-30">
          
          @if (session('status'))
          <div class="alert alert-success alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Ditambahkan.
          </div>
          @endif

          @if (session('edit'))
          <div class="alert alert-warning alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Diubah.
          </div>
          @endif

          @if (session('delete'))
          <div class="alert alert-danger alert-dismissible " role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success!</strong> Data Aset Berhasil Dihapus.
          </div>
          @endif
      </p>
      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No Aset</th>
            <th>Tanggal Permintaan Cuti</th>
            <th>Nama Karyawan</th>
            <th>Nama Atasan</th>
            <th>Jenis Cuti</th>
            <th>Tanggal Cuti</th>
            <th>Lama Cuti</th>
            <th>Action</th>
          </tr>
        </thead>


        <tbody>
          @foreach ($cuti3 as $v => $ast )
          <tr>
            <td>{{$loop -> iteration}}</td>
            <td>{{$ast -> tanggal_request}}</td>
            <td>{{$ast -> employee -> nama}}</td>
            <td>{{$ast -> employee -> nama_atasan}}</td>
            <td>{{$ast -> category -> jenis_cuti}}</td>
            <td>{{$ast -> tanggal_cuti}}</td>
            <td>{{$ast -> lama_cuti}} Hari</td>
            <td>
                <form action="/persetujuan_atasan/{{$ast -> id}}" method="post" class="d-inline">
                <a href="/persetujuan_atasan/{{$ast->id}}/edit" class="btn btn-custon-four btn-info">Persetujuan</a>
                </form>
            </td>
            

          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
  </div>
</div>


<div class="clearfix"></div>

<div class="col-md-12 col-sm-12  ">
  <div class="x_panel">
    <div class="x_title">
      <h2>History Request Cuti <small> PT Davinti Indonesia</small></h2>
      <ul class="nav navbar-right panel_toolbox">
        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
        </li>
        <li><a class="close-link"><i class="fa fa-close"></i></a>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>

    <div class="x_content">

      <p>Daftar Karyawan PT Davinti Indonesia yang pernah mengajukan cuti</p>

      <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
          <thead>
            <tr class="headings">
           
              <th>No </th>
              <th>Tanggal Request </th>
              <th>Nama Karyawan </th>
              <th>Nama Atasan </th>
              <th>Jenis Cuti </th>
              <th>Tanggal Cuti </th>
              <th>Lama Cuti </th>
             
              <th class="column-title no-link last"><span class="nobr">Status</span>
              </th>
              <th class="bulk-actions" colspan="7">
                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
              </th>
              
            </tr>
          </thead>

          <tbody>
            <tr class="even pointer">
            @foreach ($cuti1 as $v => $ast )
            <td>{{$loop -> iteration}}</td>
            <td>{{$ast -> tanggal_request}}</td>
            <td>{{$ast -> employee -> nama}}</td>
            <td>{{$ast -> employee -> nama_atasan}}</td>
            <td>{{$ast -> category -> jenis_cuti}}</td>
            <td>{{$ast -> tanggal_cuti}}</td>
            <td>{{$ast -> lama_cuti}} Hari</td>
            <td>
            <form  method="GET" class="d-inline">
            @if($ast->keputusan_hrd=='diterima'&& $ast->keputusan_direktur=='diterima'&& $ast->keputusan_atasan=='diterima') <a  href="/persetujuan_atasan/{{$ast -> id}}/detil" type="button" class="btn btn-round btn-success btn-sm text-dark"> Cuti Diterima </a> 
            @elseif($ast->keputusan_hrd=='ditolak'or $ast->keputusan_direktur=='ditolak' or $ast->keputusan_atasan=='ditolak')<a  href="/persetujuan_atasan/{{$ast -> id}}/detil" type="button" class="btn btn-round btn-danger btn-sm text-dark">Cuti Ditolak </a>
            @else <a href="/persetujuan_atasan/{{$ast -> id}}/detil" type="button" class="btn btn-round btn-warning btn-sm text-dark">Menunggu </a>@endif
            
            </form>
            </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{ $cuti1->links() }}
      </div>


    </div>
  </div>
</div>
</div>
</div>
</div>


@endsection
