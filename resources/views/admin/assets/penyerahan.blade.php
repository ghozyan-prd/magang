@extends('layouts.master')

@section('content')
<div class="page-title">
              <div class="title_left">
                <h3>Penyerahan Assets <small>By : {{auth()->user()->level}} ({{auth()->user()->nama}})</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Aplikasi Aset  <small>PT Davinti Indonesia</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a class="dropdown-item" href="#">Settings 1</a>
              </li>
              <li><a class="dropdown-item" href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form action="/transaksi" method="post" enctype="multipart/form-data" data-parsley-validate class="form-horizontal form-label-left">

          @csrf
          <div class="item form-group">
            <label class="col-form-label col-md-3 col-sm-3 label-align"  for="last-name"  >Kode Penyerahan <span >*</span>
            </label>
            <div class="col-md-6 col-sm-6 ">
              <input type="text"  name="kode_penyerahan" readonly class="form-control" placeholder="Automatic Generate">
              <input type="hidden" id="id_aset" name="id_aset" value="{{$asset -> id}}">
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Kategori</label>
            <div class="col-md-6 col-sm-6 ">
              <input class="form-control" value="{{$asset -> kategori}}" type="text" name="kategori" readonly>
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Kode Aset</label>
            <div class="col-md-6 col-sm-6 ">
              <input  class="form-control" value="{{$asset -> kode_aset}}" type="text" name="kode_aset" readonly>
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Nama Aset</label>
            <div class="col-md-6 col-sm-6 ">
              <input  class="form-control" value="{{$asset -> nama_aset}}" type="text" name="nama_aset" readonly>
            </div>
          </div>
          <div class="item form-group">
            <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tanggal Beli</label>
            <div class="col-md-6 col-sm-6 ">
              <input  class="form-control" value="{{$asset -> tanggal_beli}}" type="date" name="tanggal_beli" readonly>
            </div>
          </div>
          <br>
        </br>
        <div class="form-group row">
          <label class="col-form-label col-md-3 col-sm-3 label-align" for="last-name">Pilih Karyawan <span class="required">*</span>
          </label>
            <div class="col-md-6 col-sm-6 ">
              <select name="id_pegawai" class="form-control">
                @foreach ($employee_array as $data)
                      <option value="{{ $data->id }}" >{{ $data->nama }}</option>
                @endforeach
              </select>
          </div>
        </div>
        <div class="item form-group">
          <label for="middle-name" class="col-form-label col-md-3 col-sm-3 label-align">Tanggal Penyerahan</label>
          <div class="col-md-6 col-sm-6 ">
            <input  class="form-control" value="{{ date(' d-m-Y') }}" type="text" name="tanggal_pinjam" readonly>
            <input type="hidden"  class="form-control" name="tanggal_kembali" value="0" readonly/>
          </div>
        </div>

          <div class="ln_solid"></div>
          <div class="item form-group ">
            <div class="col-md-6 col-sm-6 offset-md-3 ">
              <button type="reset" class="btn btn-round btn-primary">Reset</button>
              <button type="submit" class="btn btn-round btn-success">Save</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>

@endsection
