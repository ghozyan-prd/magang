<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('formlogin');
});
Route::get('/formlogin','AuthController@formlogin')->name('login');
Route::post('/postlogin','AuthController@postlogin');
Route::get('/login_personal','PersonalAuthController@login_personal')->name('login');
Route::post('/postloginpersonal','PersonalAuthController@postloginpersonal');
Route::get('/formregister','AuthController@formregister');
Route::get('/logout','AuthController@logout');


Route::get('postulant','postulantController@index');



Route::group(['middleware' => ['auth','CheckLevel:Admin']],function(){

  Route::get('/home', 'HomeController@index')->name('home');
  
  Route::get('/assets/trash','AssetsController@trash');
  Route::get('/assets/restore/{id}','AssetsController@restore');
  Route::get('/assets/deleted_permanent/{id}','AssetsController@deleted_permanent');
  Route::get('/assets/restore_all','AssetsController@restore_all');
  Route::get('/assets/deleted_all','AssetsController@deleted_all');
  Route::get('/assets/{asset}/transaksi','AssetsController@transaksi');
  Route::patch('/transaksi/{asset}','AssetsController@transaksi');
  Route::post('/transaksi','AssetsController@input_transaksi');
  Route::Resource('assets', 'AssetsController');
  Route::Resource('employees', 'EmployeesController');
  //Route::Resource('asset_transactions', 'AssetTransactionController');
  //karyawan
  Route::get('/karyawanresign','ResignController@index');

  //Cuti Admin
   //Data Pengajuan Cuti
  Route::get('/cuti/trash','CutiController@trash');
  Route::get('/cuti/restore/{id}','CutiController@restore');
  Route::get('/cuti/deleted_permanent/{id}','CutiController@deleted_permanent');
  Route::get('/cuti/restore_all','CutiController@restore_all');
  Route::get('/cuti/deleted_all','CutiController@deleted_all');
  Route::Resource('cuti','CutiController');
    //Kategori Cuti
  Route::get('/category/trash','CategoryController@trash');
  Route::get('/category/restore/{id}','CategoryController@restore');
  Route::get('/category/deleted_permanent/{id}','CategoryController@deleted_permanent');
  Route::get('/category/restore_all','CategoryController@restore_all');
  Route::get('/category/deleted_all','CategoryController@deleted_all');
  Route::Resource('category','CategoryController');
 

  //gaji

  //kelola manajemen
    //jabatan
  Route::get('/jabatan/trash','JabatanController@trash');
  Route::get('/jabatan/restore/{position}','JabatanController@restore');
  Route::get('/jabatan/deleted_permanent/{position}','JabatanController@deleted_permanent');
  Route::get('/jabatan/restore_all','JabatanController@restore_all');
  Route::get('/jabatan/deleted_all','JabatanController@deleted_all');
  Route::delete('/jabatan/{position}','JabatanController@destroy');
  Route::Resource('jabatan','JabatanController');

    //divisi
  Route::get('/divisi/trash','DivisionController@trash');
  Route::get('/divisi/restore/{division}','DivisionController@restore');
  Route::get('/divisi/deleted_permanent/{division}','DivisionController@deleted_permanent');
  Route::get('/divisi/restore_all','DivisionController@restore_all');
  Route::get('/divisi/deleted_all','DivisionController@deleted_all');
  Route::delete('/divisi/{division}','DivisionController@destroy');
  Route::Resource('divisi','DivisionController');
    
    //corporate_group
  Route::get('/corporate/trash','CorporateGroupController@trash');
  Route::get('/corporate/restore/{corporate}','CorporateGroupController@restore');
  Route::get('/corporate/deleted_permanent/{corporate}','CorporateGroupController@deleted_permanent');
  Route::get('/corporate/restore_all','CorporateGroupController@restore_all');
  Route::get('/corporate/deleted_all','CorporateGroupController@deleted_all');
  Route::delete('/corporate/{corporate}','CorporateGroupController@destroy');
  Route::Resource('corporate','CorporateGroupController');
    
    //grade
  Route::get('/grade/trash','GradeController@trash');
  Route::get('/grade/restore/{grade}','GradeController@restore');
  Route::get('/grade/deleted_permanent/{grade}','GradeController@deleted_permanent');
  Route::get('/grade/restore_all','GradeController@restore_all');
  Route::get('/grade/deleted_all','GradeController@deleted_all');
  Route::delete('/grade/{grade}','GradeController@destroy');
  Route::Resource('grade','GradeController');
});






  Route::group(['middleware' => ['auth','CheckLevel:Admin,Pegawai,Atasan,Direktur']],function(){
  Route::get('/admin','AdminController@dashboard');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');






Route::group(['middleware' => ['auth','CheckLevel:Pegawai']],function(){

  Route::get('/home', 'HomeController@index')->name('home');

  Route::Resource('cutipegawai','CutiPegawaiController');
});