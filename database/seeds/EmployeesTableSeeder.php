<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('employees')->insert([
        [
          'id_postulant' => '1',
      'nama' => 'Randa',
      'kode_karyawan' => 'randa@gmail.com',
      'password' => Hash::make('1234'),
      'level' => 'Admin',
      'id_jabatan' => '1',
      'id_grade' => '1',
      'id_corporate' => '1',
      'id_devisi' => '1',
      'join_date' => '2019-12-26 00:00:00',
      'status' => 'jomblo',
      'email_corporate' => Str::random(10).'@gmail.com',
      'bank_account' => Str::random(15),
      'rekening_number' => Str::random(15),
      'account_name' => Str::random(15),
      'nama_atasan' => 'Randa',
      'jatah_cuti' => '10',
      'first_contract' => '2019-12-26 00:00:00',
      'start_addendum1' => '2019-12-26 00:00:00',
      'end_addendum1' => '2019-12-26 00:00:00',
      'start_addendum2' => '2019-12-26 00:00:00',
      'end_addendum2' => '2019-12-26 00:00:00',
      'end_contract' => '2019-12-26 00:00:00',
      'alasan_berhenti' => 'aafafa',
      'remember_token' => Str::random(15),
      'created_at' => '2019-12-26 00:00:00',
      'updated_at' => '2019-12-26 00:00:00',
        ],[
          'id_postulant' => '2',
          'nama' => 'Miko',
          'kode_karyawan' => 'pegawai@gmail.com',
          'password' => Hash::make('1234'),
          'level' => 'Pegawai',
          'id_jabatan' => '1',
          'id_grade' => '1',
          'id_corporate' => '1',
          'id_devisi' => '1',
          'join_date' => '2019-12-26 00:00:00',
          'status' => 'jomblo',
          'email_corporate' => Str::random(10).'@gmail.com',
          'bank_account' => Str::random(15),
          'rekening_number' => Str::random(15),
          'account_name' => Str::random(15),
          'nama_atasan' => 'Randa',
          'jatah_cuti' => '10',
          'first_contract' => '2019-12-26 00:00:00',
          'start_addendum1' => '2019-12-26 00:00:00',
          'end_addendum1' => '2019-12-26 00:00:00',
          'start_addendum2' => '2019-12-26 00:00:00',
          'end_addendum2' => '2019-12-26 00:00:00',
          'end_contract' => '2019-12-26 00:00:00',
          'alasan_berhenti' => 'aafafa',
          'remember_token' => Str::random(15),
          'created_at' => '2019-12-26 00:00:00',
          'updated_at' => '2019-12-26 00:00:00',

       ],[
        'id_postulant' => '3',
          'nama' => 'Dika',
          'kode_karyawan' => 'atasan@gmail.com',
          'password' => Hash::make('1234'),
          'level' => 'Atasan',
          'id_jabatan' => '1',
          'id_grade' => '1',
          'id_corporate' => '1',
          'id_devisi' => '1',
          'join_date' => '2019-12-26 00:00:00',
          'status' => 'jomblo',
          'email_corporate' => Str::random(10).'@gmail.com',
          'bank_account' => Str::random(15),
          'rekening_number' => Str::random(15),
          'account_name' => Str::random(15),
          'nama_atasan' => 'Randa',
          'jatah_cuti' => '10',
          'first_contract' => '2019-12-26 00:00:00',
          'start_addendum1' => '2019-12-26 00:00:00',
          'end_addendum1' => '2019-12-26 00:00:00',
          'start_addendum2' => '2019-12-26 00:00:00',
          'end_addendum2' => '2019-12-26 00:00:00',
          'end_contract' => '2019-12-26 00:00:00',
          'alasan_berhenti' => 'aafafa',
          'remember_token' => Str::random(15),
          'created_at' => '2019-12-26 00:00:00',
          'updated_at' => '2019-12-26 00:00:00',
       ],[
        'id_postulant' => '4',
          'nama' => 'Kelvin',
          'kode_karyawan' => 'direktur@gmail.com',
          'password' => Hash::make('1234'),
          'level' => 'Direktur',
          'id_jabatan' => '1',
          'id_grade' => '1',
          'id_corporate' => '1',
          'id_devisi' => '1',
          'join_date' => '2019-12-26 00:00:00',
          'status' => 'jomblo',
          'email_corporate' => Str::random(10).'@gmail.com',
          'bank_account' => Str::random(15),
          'rekening_number' => Str::random(15),
          'account_name' => Str::random(15),
          'nama_atasan' => 'Randa',
          'jatah_cuti' => '10',
          'first_contract' => '2019-12-26 00:00:00',
          'start_addendum1' => '2019-12-26 00:00:00',
          'end_addendum1' => '2019-12-26 00:00:00',
          'start_addendum2' => '2019-12-26 00:00:00',
          'end_addendum2' => '2019-12-26 00:00:00',
          'end_contract' => '2019-12-26 00:00:00',
          'alasan_berhenti' => 'aafafa',
          'remember_token' => Str::random(15),
          'created_at' => '2019-12-26 00:00:00',
          'updated_at' => '2019-12-26 00:00:00',
       ]
  ]);

    }
}
