<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PostulantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('postulants')->insert([
          [
          'nama_lengkap' => 'Randa Wahyu',
          'posisi_yang_dilamar' => 'copy writer',
          'tempat_lahir' => 'Surabaya',
          'tanggal_lahir' => '1998-03-21 00:00:00',
          'no_ktp' => Str::random(15),
          'jenis_kelamin' => 'laki',
          'status_perkawinan' => 'menikah',
          'agama' => 'Islam',
          'kebangsaan' => 'Islam',
          'tanggal_join' => '1998-03-21 00:00:00',
          'npwp' => '081273338544',
          'alamat_domisili' => 'Surabaya',
          'alamat_ktp' => 'Surabaya',
          'telepon' => '081273338544',
          'email' => 'jomblo',
          'kendaraan' => 'Surabaya',
          'laptop' => 'ada',
          'gaji_diharap' => '500000',
          'anak_ke' => '1',
          'saudara' => '2',
          'pencapaian_yang_diinginkan' => 'Sarjana',
          'pencapaian_sudah_dilakukan' => 'Sarjana',
          'foto' => Str::random(15),
          'catatan' => 'ini catatan interview',
          'status_lamaran' => 'diterima',
          'alasan' => 'kepo',
          'created_at' => '2019-12-26 00:00:00',
          'updated_at' => '2019-12-26 00:00:00',
          ],[
            'nama_lengkap' => 'Miko Setiawan',
            'posisi_yang_dilamar' => 'copy writer',
            'tempat_lahir' => 'Surabaya',
            'tanggal_lahir' => '1998-03-21 00:00:00',
            'no_ktp' => Str::random(15),
            'jenis_kelamin' => 'laki',
            'status_perkawinan' => 'menikah',
            'agama' => 'Islam',
            'kebangsaan' => 'Islam',
            'tanggal_join' => '1998-03-21 00:00:00',
            'npwp' => '081273338544',
            'alamat_domisili' => 'Surabaya',
            'alamat_ktp' => 'Surabaya',
            'telepon' => '081273338544',
            'email' => 'jomblo',
            'kendaraan' => 'Surabaya',
            'laptop' => 'ada',
            'gaji_diharap' => '500000',
            'anak_ke' => '1',
            'saudara' => '2',
            'pencapaian_yang_diinginkan' => 'Sarjana',
            'pencapaian_sudah_dilakukan' => 'Sarjana',
            'foto' => Str::random(15),
            'status_lamaran' => 'diterima',
            'alasan' => 'kepo',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',

          ],[
            'nama_lengkap' => 'Dika Ramadhani',
          'posisi_yang_dilamar' => 'copy writer',
          'tempat_lahir' => 'Surabaya',
          'tanggal_lahir' => '1998-03-21 00:00:00',
          'no_ktp' => Str::random(15),
          'jenis_kelamin' => 'laki',
          'status_perkawinan' => 'menikah',
          'agama' => 'Islam',
          'kebangsaan' => 'Islam',
          'tanggal_join' => '1998-03-21 00:00:00',
          'npwp' => '081273338544',
          'alamat_domisili' => 'Surabaya',
          'alamat_ktp' => 'Surabaya',
          'telepon' => '081273338544',
          'email' => 'jomblo',
          'kendaraan' => 'Surabaya',
          'laptop' => 'ada',
          'gaji_diharap' => '500000',
          'anak_ke' => '1',
          'saudara' => '2',
          'pencapaian_yang_diinginkan' => 'Sarjana',
          'pencapaian_sudah_dilakukan' => 'Sarjana',
          'foto' => Str::random(15),
          'status_lamaran' => 'diterima',
          'alasan' => 'kepo',
          'created_at' => '2019-12-26 00:00:00',
          'updated_at' => '2019-12-26 00:00:00',
          ],[
            'nama_lengkap' => 'Niken Andriani',
          'posisi_yang_dilamar' => 'copy writer',
          'tempat_lahir' => 'Surabaya',
          'tanggal_lahir' => '1998-03-21 00:00:00',
          'no_ktp' => Str::random(15),
          'jenis_kelamin' => 'laki',
          'status_perkawinan' => 'menikah',
          'agama' => 'Islam',
          'kebangsaan' => 'Islam',
          'tanggal_join' => '1998-03-21 00:00:00',
          'npwp' => '081273338544',
          'alamat_domisili' => 'Surabaya',
          'alamat_ktp' => 'Surabaya',
          'telepon' => '081273338544',
          'email' => 'jomblo',
          'kendaraan' => 'Surabaya',
          'laptop' => 'ada',
          'gaji_diharap' => '500000',
          'anak_ke' => '1',
          'saudara' => '2',
          'pencapaian_yang_diinginkan' => 'Sarjana',
          'pencapaian_sudah_dilakukan' => 'Sarjana',
          'foto' => Str::random(15),
          'status_lamaran' => 'diterima',
          'alasan' => 'kepo',
          'created_at' => '2019-12-26 00:00:00',
          'updated_at' => '2019-12-26 00:00:00',
          ]
      ]);
    }
}
