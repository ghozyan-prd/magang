<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EmergencycontactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emergencycontacts')->insert([
            [
            'id_postulant' => '1',
            'nama' => 'dwi',
            'hubungan' => 'Kepala',
            'alamat' => 'jaan tandes lor sby',
            'telepon' => '0823232424',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],
            [
            'id_postulant' => '1',
            'nama' => 'Miko',
            'hubungan' => 'saudara',
            'alamat' => 'jaan tandes lor sby',
            'telepon' => '0823232424',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],

        ]);
    }
}
