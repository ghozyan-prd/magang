<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('assets')->insert([
        [
            'kode_aset' => 'AS01',
            'nama_aset' => 'spanish',
            'kategori' => 'Kepala Gugus',
            'tanggal_beli' => '2019-12-26 00:00:00',
            'gambar' => 'ewdwdwdw',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],
            [
            'kode_aset' => 'AS02',
            'nama_aset' => 'spanish',
            'kategori' => 'Kepala Gugus',
            'tanggal_beli' => '2019-12-26 00:00:00',
            'gambar' => 'wdwdw',
            'created_at' => '2019-12-26 00:00:00',
            'updated_at' => '2019-12-26 00:00:00',
            ],

        ]);
    }
}
