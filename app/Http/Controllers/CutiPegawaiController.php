<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use App\Cuti;
use App\Employee;
use App\Category;
use Illuminate\Http\Request;

class CutiPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuti4 = Cuti::where('id_karyawan',auth()->User()->id)->paginate(15);
       
       
        return view('employee.cuti.index', ['cuti4'=>$cuti4,'category'=>Category::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = Category::find($request->jenis_cuti);
        $value=($id->value);
        Cuti::create([
            'tanggal_request' =>date('Y-m-d'),
            'id_karyawan' => auth()->User()->id,
            'nama_atasan' => auth()->User()->nama_atasan,
            'id_cuti' =>  $request->jenis_cuti,
            'tanggal_cuti' => $request->tanggal_cuti,
            'lama_cuti' => $request->lama_cuti,
            'alasan' => $request->alasan,
            'cuti_terpakai' => $request->lama_cuti* $value,
            'keputusan_atasan' => 'menunggu',
            'keputusan_hrd' => 'menunggu',
            'keputusan_direktur' => 'menunggu',
           
    
          ]);
          //dd($request);
          return redirect('/cutipegawai')-> with('status', 'Data Aset Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function detil(Cuti $cuti)
    {
     
        return view('employee.cuti.detil', compact('cuti'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
