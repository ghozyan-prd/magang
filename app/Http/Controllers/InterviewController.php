<?php

namespace App\Http\Controllers;


use App\Family;
use App\Interview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interview1 = Interview::where('status_lamaran','=',null)
        -> orWhere('status_lamaran','=','')
        -> get();

        $interview2 = Interview::where('status_lamaran','=','Diterima')
        -> orWhere('status_lamaran','=','Ditolak : Good')
        -> orWhere('status_lamaran','=','Ditolak : Bad')
        -> get();

        return view('admin.interview.index', compact('interview1','interview2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function show(Interview  $interview)
    {
        // dd($interview);

        // $family = DB::table('familys')
        //             ->select('id_postulant','hubungan', 'nama', 'jenis_kelamin', 'usia', 'pendidikan', 'pekerjaan')
        //             ->where('id_postulant', $interview->id)
        //             ->get();

        $family = Family::where('id_postulant', $interview->id)
                    ->get();
        
        // dd($family);
        
        return view('admin.interview.show', compact('interview', 'family'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function edit(Interview  $interview)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Interview::where('id',$request->idIntv)
        ->update([
            'catatan' => $request->catatanIntv,
            'status_lamaran' => $request->keputusanIntv,
            'alasan' => $request->alasanIntv
        ]);
        return redirect('/interview')-> with('edit', 'Data Grade Berhasil Diubah !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Interview  $interview
     * @return \Illuminate\Http\Response
     */
    public function destroy(Interview  $interview)
    {
        Interview::destroy($interview->id);
        return redirect('/interview')-> with('delete', 'Data Grade Berhasil Dihapus');
    }

    public function trash(){
        $interview = Interview::onlyTrashed()->get();
        return view ('admin.interview.trash', ['interviews' => $interview]);
      }

    public function restore($id){
        $interview = Interview::onlyTrashed()->where('id', $id);
        $interview->restore();
        return redirect('/interview/trash')-> with('restore', 'Data Grade Berhasil di Restore');
      }
  
    public function deleted_permanent($id){
        $interview = Interview::onlyTrashed()->where('id', $id);
        $interview->forceDelete();
  
        return redirect('/interview/trash')-> with('delete', 'Data Grade Berhasil di Delete Permanent');
      }
  
    public function restore_all(){
        $interview = Interview::onlyTrashed();
        $interview->restore();
  
        return redirect('/interview/trash')-> with('restore_all', 'Data Grade Berhasil di Restore all');
      }
  
    public function deleted_all(){
        $interview = Interview::onlyTrashed();
        $interview->forceDelete();
  
        return redirect('/interview/trash')-> with('deleted_all', 'Data Grade Berhasil di Delete Semua');
      }
}
