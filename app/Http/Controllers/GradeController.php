<?php

namespace App\Http\Controllers;

use App\Grade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grades = Grade::all();
        return view ('admin/grade.index',compact('grades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namaGrd' => 'required'
        ]);
    
        $id = DB::table('grades')->orderBy('id','DESC')->take(1)->get();
        foreach ($id as $value);
        $idlama = $value->id;
        $idbaru = $idlama + 1;
        $kode_grade = 'Grd-'.$idbaru;
    
        Grade::create([
            'kode_grade' => $kode_grade,
            'nama_grade' => $request->namaGrd,
        ]);
        //dd($request);    
        return redirect('/grade')-> with('status', 'Data Grade Baru Berhasil di Tambahkan !!');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function show(Grade $grade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function edit(Grade $grade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request->all());
        
        $request->validate([
            'namaGrd' => 'required'
        ]);

        Grade::where('id',$request->idIntv)
        ->update([
            'status_lamaran' => $request->keputusanIntv
        ]);
        
        return redirect('/interview')-> with('edit', 'Data Interview Berhasil Diubah !!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grade  $grade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grade $grade)
    {
        Grade::destroy($grade->id);
        return redirect('/grade')-> with('delete', 'Data Grade Berhasil Dihapus');
    }

    public function trash(){
        $grade = Grade::onlyTrashed()->get();
        return view ('admin.grade.trash', ['grades' => $grade]);
      }

    public function restore($id){
        $grade = Grade::onlyTrashed()->where('id', $id);
        $grade->restore();
        return redirect('/grade/trash')-> with('restore', 'Data Grade Berhasil di Restore');
      }
  
    public function deleted_permanent($id){
        $grade = Grade::onlyTrashed()->where('id', $id);
        $grade->forceDelete();
  
        return redirect('/grade/trash')-> with('delete', 'Data Grade Berhasil di Delete Permanent');
      }
  
    public function restore_all(){
        $grade = Grade::onlyTrashed();
        $grade->restore();
  
        return redirect('/grade/trash')-> with('restore_all', 'Data Grade Berhasil di Restore all');
      }
  
    public function deleted_all(){
        $grade = Grade::onlyTrashed();
        $grade->forceDelete();
  
        return redirect('/grade/trash')-> with('deleted_all', 'Data Grade Berhasil di Delete Semua');
      }
}
