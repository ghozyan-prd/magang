<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;

class PersonalAuthController extends Controller
{
  public function login_personal(){
    return view ('login_personal');
  }

  public function formregister(){
    return view ('formregister');
  }

  public function postloginpersonal(Request $request){
    if(Auth::attempt($request->only('email'))){
      return redirect('personals');
      dd($request);
    }
    return redirect('login_personal');
  }

  public function logout_personal(){
    Auth::logout();
    return redirect('login_personal');
  }
}
