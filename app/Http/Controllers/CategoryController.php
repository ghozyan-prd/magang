<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('admin.category.index', compact('category'));       
    }
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jenisCc' => 'required',
            'ketCc' => 'required',
            'valueCc' => 'required'
        ]);
        
        $id = DB::table('cuti_categorys')->orderBy('id','DESC')->take(1)->get();
        foreach ($id as $value);
        $idlama = $value->id;
        $idbaru = $idlama + 1;
        $kode_cc = 'CC-'.$idbaru;
        
        Category::create([
            'kode_kategori_cuti' => $kode_cc,
            'jenis_cuti' => $request->jenisCc,
            'keterangan' => $request->ketCc,
            'value' => $request->valueCc
            ]);
        //dd($request);    
        return redirect('/category')-> with('status', 'Data Katergori Cuti Baru Berhasil di Tambahkan !!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {        
        //dd($request->all());
        
        $request->validate([
            'jenisCc' => 'required',
            'ketCc' => 'required',
            'valueCc' => 'required',
        ]);

        Category::where('id',$request->idCc)
        ->update([
            'jenis_cuti' => $request->jenisCc,
            'keterangan' => $request->ketCc,
            'value' => $request->valueCc,
        ]);

        return redirect('category')->with('edit', 'Data Karyawan Berhasil Diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        Category::destroy($category->id);
        return redirect('/category')-> with('delete', 'Data Aset Berhasil di Hapus');
    }

    public function trash(){
        $category = Category::onlyTrashed()->get();
        return view ('admin.category.trash', ['categorys' => $category]);
      }

    public function restore($id){
        $category = Category::onlyTrashed()->where('id', $id);
        $category->restore();
        return redirect('/category/trash')-> with('restore', 'Data Category Berhasil di Restore');
      }
  
    public function deleted_permanent($id){
        $category = Category::onlyTrashed()->where('id', $id);
        $category->forceDelete();
  
        return redirect('/category/trash')-> with('delete', 'Data category Berhasil di Delete Permanent');
      }
  
    public function restore_all(){
        $category = Category::onlyTrashed();
        $category->restore();
  
        return redirect('/category/trash')-> with('restore_all', 'Data category Berhasil di Restore all');
      }
  
    public function deleted_all(){
        $category = Category::onlyTrashed();
        $category->forceDelete();
  
        return redirect('/category/trash')-> with('deleted_all', 'Data Grade Berhasil di Delete Semua');
      }
}
