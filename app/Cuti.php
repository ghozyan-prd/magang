<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $table = "cuti_requests";
    protected $fillable = ['tanggal_request', 'id_karyawan', 'nama_atasan','id_cuti','tanggal_cuti', 'lama_cuti', 'alasan','cuti_terpakai','catatan_atasan','catatan_hrd','catatan_direktur','keputusan_direktur','keputusan_hrd','keputusan_atasan'];
    public function category(){
		return $this ->belongsTo('App\Category','id_cuti');
    }

    public function employee(){
		return $this ->belongsTo('App\Employee','id_karyawan');
    }
}
