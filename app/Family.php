<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    protected $table = "familys";

    public function interview(){
		    return $this ->belongsTo(Interview::class, 'id_postulant');
    }
}
