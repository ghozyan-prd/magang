<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interview extends Model
{
    use SoftDeletes;
    protected $table = "postulants";
    protected $fillable = ['catatan', 'status_lamaran', 'alasan'];
    public function familys(){  
        return $this ->hasMany(Family::class, 'id_postulant');
    }    
}