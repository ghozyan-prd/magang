<?php

namespace App\Providers;
use App\Employee; // write model name here
use App\Asset;
use Illuminate\Support\ServiceProvider;

class DynamicEmployee extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view){
        $view->with('employee_array', Employee::all());
      });
    }

    public function boot1()
    {
        view()->composer('*',function($view){
        $view->with('transaksi', Asset::all());
      });
    }
}
