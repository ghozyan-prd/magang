<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Position extends Model
{
    use SoftDeletes;
    protected $table = "positions";
    protected $fillable = ['kode_jabatan', 'nama_jabatan'];
}
